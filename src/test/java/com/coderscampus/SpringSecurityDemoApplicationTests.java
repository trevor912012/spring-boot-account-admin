package com.coderscampus;

import java.util.List;

import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.coderscampus.domain.Role;
import com.coderscampus.domain.User;
import com.coderscampus.service.RoleService;
import com.coderscampus.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringSecurityDemoApplicationTests {
	
	@Autowired
	RoleService roleService;
	@Autowired
	UserService userService;
	@Test
	public void saveRole() {
//		Role role = new Role();
//		role.setName("ROLE_dba");
//		role.setNameZh("Database Administrator");
//		roleService.saveRole(role);
//		
//		role = new Role();
//		role.setName("ROLE_admin");
//		role.setNameZh("System Manager");
//		roleService.saveRole(role);
//		
//		role = new Role();
//		role.setName("ROLE_user");
//		role.setNameZh("General Users");
//		roleService.saveRole(role);
	}
	
	@Test
	public void saveUser() {
//		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//		User user = new User();
//		user.setUsername("dba");
//		user.setPassword(passwordEncoder.encode("1234"));
//		user.setEnabled(true);
//		user.setLocked(false);
//		userService.saveUser(user);
//		
//		user = new User();
//		user.setUsername("admin");
//		user.setPassword(passwordEncoder.encode("1234"));
//		user.setEnabled(true);
//		user.setLocked(false);
//		userService.saveUser(user);
//		
//		user = new User();
//		user.setUsername("trevor");
//		user.setPassword(passwordEncoder.encode("1234"));
//		user.setEnabled(true);
//		user.setLocked(false);
//		userService.saveUser(user);
	}
	@Test
	public void associationUser() {
		userService.associationRole(73L, 70L);
		userService.associationRole(74L, 71L);
		userService.associationRole(75L, 72L);
	}
	
	@Test
	@Transactional
	public void findUser() {
//		User user = userService.findByUserName("trevor");
//		user = userService.findByUserId(user.getId());
//		List<Role> roleList = user.getRole();
//		System.out.println(String.format("测试结果为：%s",roleList.get(0).getName()));
	}
}
