package com.coderscampus.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.coderscampus.domain.Role;
import com.coderscampus.domain.User;

public interface UserReopsitory extends JpaRepository<User,Long>{
	@Query("select u from User u"
		      + " where u.username = :username")
	User findByUsername(String username);
	
//	List<Role> findRolesByUserId(Long userId);
}
