package com.coderscampus.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	@GetMapping("/dba/hello")
	public String dba() {
		return "Hello dba";
	}
	@GetMapping("/admin/hello")
	public String admin() {
		return "Hello admin";
	}
	@GetMapping("/user/hello")
	public String user() {
		return "Hello user";
	}
	@GetMapping("/dashboard")
	  public String getDashboard () {
	    return "dashboard";
	  }
}
