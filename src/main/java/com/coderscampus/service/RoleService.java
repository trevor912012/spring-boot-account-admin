package com.coderscampus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.coderscampus.domain.Role;
import com.coderscampus.domain.User;
import com.coderscampus.repository.RoleReopsitory;
import com.coderscampus.repository.UserReopsitory;
@Service
public class RoleService {
	@Autowired
	private RoleReopsitory roleReopsitory;
	
	@Autowired
	private UserReopsitory userReopsitory;
	
	@Transactional
	public Role saveRole(Role role) {
		// TODO Auto-generated method stub
		return roleReopsitory.save(role);
	}
	
	public Role findRoleById(Long roleId) {
		// TODO Auto-generated method stub
		return roleReopsitory.getOne(roleId);
	}
}
