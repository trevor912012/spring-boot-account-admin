package com.coderscampus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.coderscampus.domain.Role;
import com.coderscampus.domain.User;
import com.coderscampus.repository.RoleReopsitory;
import com.coderscampus.repository.UserReopsitory;

@Service
public class UserService implements UserDetailsService{
	@Autowired
	private UserReopsitory userReopsitory;
	@Autowired
	private RoleReopsitory roleReopsitory;
	@Transactional
	public User saveUser(User user) {
		// TODO Auto-generated method stub
		return userReopsitory.save(user);
	}
	
	public User findByUserName(String username) {
		return userReopsitory.findByUsername(username);
	}
	
	public User findByUserId(Long userId) {
		return userReopsitory.getOne(userId);
	}
	
	@Transactional
	public User associationRole(Long userId, Long roleId) {
		Role role = roleReopsitory.getOne(roleId);
		User user = userReopsitory.getOne(userId);
		user.getRole().add(role);
		return userReopsitory.save(user);
	}
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = this.findByUserName(username);
		if(user == null) {
			throw new UsernameNotFoundException("Account is not exit");
		}
		user = this.findByUserId(user.getId());
		return user;
	}
}
