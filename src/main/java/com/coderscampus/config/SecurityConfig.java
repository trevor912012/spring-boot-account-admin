package com.coderscampus.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.coderscampus.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	UserService userService;
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.inMemoryAuthentication()
//									.withUser("admin").password("$2a$10$1nuDfCm10l.Kn/.yo16.U.7fRvYa0Wh.yDwuEpWwp635HStHxVxSe").roles("admin")
//									.and()
//									.withUser("trevor").password("$2a$10$1nuDfCm10l.Kn/.yo16.U.7fRvYa0Wh.yDwuEpWwp635HStHxVxSe").roles("user");
		auth.userDetailsService(userService);
	}
	
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers("/dba/**").hasRole("dba")
			.antMatchers("/admin/**").hasRole("admin")
			.antMatchers("/user/**").hasRole("user")
			.anyRequest().authenticated()
			.and()
			.formLogin()
			.loginProcessingUrl("/login").defaultSuccessUrl("/dashboard").permitAll()
			.and()
			.csrf().disable();
//		http.authorizeRequests()
//		.antMatchers("/admin/**")
//		.hasRole("ADMIN")
//		.antMatchers("/user/**")
//		.access("hasAnyRole('ADMIN','USER')")
//		.antMatchers("/db/**")
//		.access("hasRole('ADMIN') and hasRole('DBA')")
//		.anyRequest()
//		.authenticated()
//		.and()
//		.formLogin()
//		.loginProcessingUrl("/login")
//		.usernameParameter("name")
//		.passwordParameter("passwd")
//		.successHandler(new AuthenticationSuccessHandler() {
//			@Override
//			public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
//					Authentication authentication) throws IOException, ServletException {
//				Object principal = authentication.getPrincipal();
//				response.setContentType("application/jason=charset=utf-8");
//				PrintWriter out = response.getWriter();
//				response.setStatus(200);
//				Map<String, Object> map = new HashMap<>();
//				map.put("status", 200);
//				map.put("msg", principal);
//				ObjectMapper om = new ObjectMapper();
//				out.write(om.writeValueAsString(map));
//				out.flush();
//				out.close();
//			}
//		})
//		.failureHandler(new AuthenticationFailureHandler() {
//			@Override
//			public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
//					AuthenticationException exception) throws IOException, ServletException {
//				response.setContentType("application/jason=charset=utf-8");
//				PrintWriter out = response.getWriter();
//				response.setStatus(401);
//				Map<String, Object> map = new HashMap<>();
//				map.put("status", 401);
//				if(exception instanceof LockedException) {
//					map.put("msg", "帳號被鎖定，登陸失敗!");
//				}else if(exception instanceof BadCredentialsException) {
//					map.put("msg", "帳號或密碼輸入錯誤，登陸失敗!");
//				}else if(exception instanceof DisabledException) {
//					map.put("msg", "帳號被禁用，登陸失敗!");
//				}else if(exception instanceof AccountExpiredException) {
//					map.put("msg", "帳號已過期，登陸失敗!");
//				}else if(exception instanceof CredentialsExpiredException) {
//					map.put("msg", "密碼已過期，登陸失敗!");
//				}else {
//					map.put("msg", "登陸失敗!");
//				}
//				ObjectMapper om = new ObjectMapper();
//				out.write(om.writeValueAsString(map));
//				out.flush();
//				out.close();	
//			}
//		})
//		.permitAll()
//		.and()
//		.csrf()
//		.disable();
	}
}
